export interface User {
    id?:number;
    email:string;
    password?:string;
    role?:string;
}

export interface Picture {
    id?:number;
    title:string;
    image:string;
    description:string;
    createdAt?: string;
    author?:User;
    imageLink?:string;
    thumbnailLink?:string;
}

export interface Comment {
    id?:number;
    content:string;
    author?:User;
    picture?:Picture;
    createdAt:string;
}
