import axios from "axios";
import type { Picture } from "./entities";



export async function fetchAllPictures() {
    const response = await axios.get<Picture[]>('/api/picture');
    return response.data;
}

export async function fetchPicture(id: any) {
    const response = await axios.get<Picture>('/api/picture/'+id);
    return response.data;
}
export async function postPicture(picture:Picture) {
    const response = await axios.post<Picture>('/api/picture', picture);
    return response.data;
}

export async function updatePicture(picture:Picture) {
    const response = await axios.put<Picture>('/api/picture/'+picture.id, picture);
    return response.data;
}
export async function deletePicture(id:any) {
    await axios.delete<void>('/api/picture/'+id);
   
}

export async function fetchMyPictures() {
    const response = await axios.get<Picture[]>('/api/account/picture');
    return response.data;
}

export async function postUpload(image:File) {
    const response = await axios.postForm<string>('/api/picture/upload', {
        image
    });
    return response.data;
}